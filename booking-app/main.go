package main

import ("fmt"
	"time"
	"sync"
	"booking-app/helper"
)
var conferenceName = "Go Conference"
const conferenceTickets int= 50
var remainingTickets uint= 50
var bookings = make([]userData, 0)

type userData struct{
	firstName string
	lastName string
	email string
	numberOfTickets uint
}

var wg = sync.WaitGroup{}

func main() {

	
	greetUsers()

	// for {
		firstName, lastName, email, userTickets := getUserInput()
		isValidName, isValidEmail, isValidTicketNumber := helper.ValidateUserInput(firstName, lastName, email, userTickets, remainingTickets)
		

		if isValidName  && isValidEmail && isValidTicketNumber {
			
			bookTicket(userTickets, firstName, lastName, email)

			wg.Add(1)
			go sendTicket(userTickets, firstName, lastName, email)
			
			
			firstNames := GetfirstNames()
			fmt.Printf("the first names of bookings are: %v\n", firstNames)
			// fmt.Printf("the whole slice: %v\n", bookings)
			// fmt.Printf("the first value: %v\n", bookings[0])
			// fmt.Printf("slice type: %T\n", bookings)
			// fmt.Printf("slice length: %v\n", len(bookings))
	
	
			if remainingTickets == 0 {
				//end program
				fmt.Println("our conference is booked out.come back next year.")
				// break
			}
		} else {
				if !isValidName{
					fmt.Println("first name or last name you entered is too short")
				}
				if !isValidEmail {
					fmt.Println("email address you entered doesn't contain @sign")
				}
				if !isValidTicketNumber {
					fmt.Println("number of tickets you entered is invalid")
				}
				}
				wg.Wait()
		}

func greetUsers(){
	fmt.Println("welcome to %v booking aplication", conferenceName)
	fmt.Printf("we have total of %v tickets and %v are still avalable.\n", conferenceTickets, remainingTickets)
	fmt.Println("get your tickets here to attend")
}

func GetfirstNames() []string {
	firstNames := []string{}
	for _, booking := range bookings {
		firstNames = append(firstNames, booking.firstName)
	}
	return firstNames
}



func getUserInput() (string, string, string, uint) {
		var firstName string
		var lastName string
		var email string
		var userTickets uint

		fmt.Println("Enter your first name: ")
		fmt.Scan(&firstName)
		
		fmt.Println("Enter your Last name: ")
		fmt.Scan(&lastName)
		
		fmt.Println("Enter your email: ")
		fmt.Scan(&email)

		fmt.Println("Enter number of tickets: ")
		fmt.Scan(&userTickets)
		return firstName, lastName, email, userTickets
}

func bookTicket(userTickets uint, firstName string, lastName string, email string){
		remainingTickets = remainingTickets -userTickets
		//creat a map
		var userData = userData {
			firstName: firstName,
			lastName: lastName,
			email: email,
			numberOfTickets: userTickets,
		}

		bookings = append(bookings, userData)
		fmt.Printf("list of booking is %v\n", bookings)

		fmt.Printf("thank you %v %v for booking %v tickets. you will receive a confirmation email at %v\n", firstName, lastName,userTickets, email,)
		fmt.Printf("%v tickets remaining for %v\n", remainingTickets, conferenceName)
}

func sendTicket( userTickets uint, fristName string, lastName string, email string) {
	time.Sleep(20 * time.Second)
	var ticket = fmt.Sprintf("%v tickets for %v %v ", userTickets, fristName, lastName)
	fmt.Println("###########")
	fmt.Printf("sending ticket:\n %v \nto email addres %v\n", ticket, email)
	fmt.Println("##########")
	wg.Done()
}